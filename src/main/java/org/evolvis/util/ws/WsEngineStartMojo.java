package org.evolvis.util.ws;

import java.io.File;

import org.apache.axis2.transport.SimpleAxis2Server;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

/**
 * 
 * 
 * @goal start-engine
 * 
 * @author Hendrik Helwich
 */
public class WsEngineStartMojo extends AbstractMojo {


	protected static final String ENGINETYPE_AXIS2 = "axis2";
	
	/**
     * @parameter
     */
    private String engineType;

	/**
     * @parameter
     */
    private String axis2Config;

	/**
     * @parameter
     */
    private String axis2Repository;
    
	
    public void execute() throws MojoExecutionException {
    	if (ENGINETYPE_AXIS2.equals(engineType))
    		startAxis2Engine();
    	// add other engines here
    	else
    		getLog().error("unknown engine type: "+engineType);
    }

	private void startAxis2Engine() throws MojoExecutionException {
    	File f = new File(axis2Config);
    	if (! f.exists() || ! f.isFile()) {
    		getLog().error("unable to find axis2 config file: "+axis2Config);
    		return;
    	}
    	String configFilename = f.getAbsolutePath();
    	f = new File(axis2Repository);
    	if (! f.exists() || ! f.isDirectory()) {
    		getLog().error("unable to find axis2 repository: "+axis2Config);
    		return;
    	}
    	String repoFilename = f.getAbsolutePath();
    	String[] args = new String[] {
    			"-repo", repoFilename,
    			"-conf", configFilename
    	};
    	try {
    		getLog().info("starting axis2 server");
			SimpleAxis2Server.main(args);
		} catch (Exception e) {
			throw new MojoExecutionException("error starting axis2 server", e);
		}
	}

}
